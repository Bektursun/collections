package com.example.kotlincollections.map

fun main() {
    val httpHeaders = mutableMapOf(
        "Authorization" to "your-api-key",
        "ContentType" to "application/json",
        "UserLocate" to "KG"
    )
    httpHeaders["UserLocate"] = "USA"
    println(httpHeaders)
    httpHeaders["Hello"] = "World"

    httpHeaders.forEach { (key, value) -> println("key = $key, value = $value") }
}