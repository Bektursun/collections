package com.example.kotlincollections.set

data class Worker(
    val id: Int,
    val name: String
)

fun main() {
    val workers = mutableSetOf(
        Worker(id = 6, name = "Nurda"),
        Worker(id = 5, name = "Bolot"),
        Worker(id = 7, name = "Beka"),
        Worker(id = 5, name = "Bolot")
    )
    println(workers)
    val removedWorker = Worker(id = 5, name = "Bolot")
    workers.remove(removedWorker)
    println(workers)
}
