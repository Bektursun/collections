package com.example.kotlincollections.array

fun main() {
    val arrayOfNumbers = arrayOf(2, 3, 4, 5, 6, 7, 8, 9)

    val someOtherArray = Array(5){ "n" }
    someOtherArray.forEach { n -> println(n) }

    cycleWhile(arrayOfNumbers)
    cycleFor(arrayOfNumbers)
    asc()
}

private fun cycleFor(array: Array<Int>) {
    for (number in array) println(number)
}

private fun cycleWhile(array: Array<Int>) = array.forEach { number -> println(number) }

private fun asc() {
    val ar = Array(5) { n -> (n * n).toString()}
    ar.forEach { println(it) }
}
