package com.example.kotlincollections.operations


data class Product(
    val id: Int,
    val name: String,
    val price: Double
)

data class Receipt(
    val id: Int,
    val seller: Worker,
    val products: List<Product>,
    val isPaid: Boolean = false
)

class Store(
    val receipts: List<Receipt>,
    val workers: List<Worker>
)

data class Worker(
    val id: Int,
    val name: String
)

fun beer() = Product(id = 2, name = "Beer, light, 0.5l", price = 6.5)
fun coffee() = Product(id = 3, name = "Ground coffee 1kg", price = 5.6)
fun bread() = Product(id = 4, name = "Gluten-free bread 1kg", price = 3.4)

fun main() {
    val firstWorker = Worker(id = 1, name = "Bolot")
    val secondWorker = Worker(id = 2, name = "Nurda")

    val store = Store(
        receipts = listOf(
            Receipt(id = 1,
                seller = firstWorker,
                products = listOf(bread(), bread(), bread(), coffee(), beer()),
                isPaid = true),
            Receipt(
                id = 2,
                seller = secondWorker,
                products = listOf(bread(), beer(), coffee(), coffee(), beer()),
                isPaid = false
            )
        ),
        workers = listOf(firstWorker, secondWorker)
    )

    val receipts = store.receipts
    val productList = receipts.flatMap { it.products }
    val prod = receipts.map { it.products }

    // sum of products earning
    getSumOfProductsEarning(receipts)

    // filtering by condition
    filteringByCondition(receipts)

    // grouping values by condition
    groupingValuesByCondition(receipts)

    // grouped by worker
    groupByWorker(receipts)

    areAllReceiptsPaid(receipts)

}

private fun getSumOfProductsEarning(receipt: List<Receipt>) {
    val allProductsEarning = receipt.flatMap { it.products }
        .map { it.price }
        .sumByDouble { it }
    println(allProductsEarning)
}

private fun filteringByCondition(receipt: List<Receipt>) {
    val paidReceipts = receipt.filter { it.isPaid }
    println("filtering by condition: $paidReceipts")
}

private fun groupByWorker(receipt: List<Receipt>) {
    val groupedByWorker = receipt.groupBy { it.seller }
    println("grouped workers: $groupedByWorker")
}

private fun groupingValuesByCondition(receipt: List<Receipt>) {
    val paidUnpaid = receipt.partition { it.isPaid }
    val (paid, unpaid) = paidUnpaid
    println("paid: $paid")
    println("unpaid: $unpaid")
}

private fun areAllReceiptsPaid(receipt: List<Receipt>) {
    val areThereNoReceipts = receipt.isEmpty()
    val areAllPaid = receipt.all { it.isPaid }
    val nonePaid = receipt.none { it.isPaid }
    val isAtLeastOnePaid = receipt.any {it.isPaid}
    println("areThereNoReceipts: $areThereNoReceipts")
    println("areAllPaid: $areAllPaid")
    println("nonePaid: $nonePaid")
    println("isAtLeastOnePaid: $isAtLeastOnePaid")
}

private fun lookingUpData(receipt: List<Receipt>) {
    val receiptByIndex = receipt[0]
    val firstPaidReceipt = receipt.first { it.isPaid }
    val firstPaidReceiptOrNull = receipt.firstOrNull { it.isPaid }
    val lastByPredicate = receipt.last { !it.isPaid }
}
