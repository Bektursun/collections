package com.example.kotlincollections.list

fun main() {
    val list = listOf(2, 3, 6, 6, 8)
    list.forEach { n -> print(n) }

    val mutableList = mutableListOf(1, 2, 4, 5, 7, 9, 12)
    mutableList.forEach { n -> println(n) }
    mutableList[1] = 12
    mutableList.add(122)
}
